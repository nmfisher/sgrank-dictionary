(defproject com.avinium/dictionary "0.0.2-SNAPSHOT"
  :description "Dictionary loader and related parsers for use with the SGRank algorithm"
  :dependencies [
                  [org.clojure/clojure "1.8.0"]
                  [org.clojure/tools.cli "0.3.5"]
                  [org.clojure/tools.logging "0.3.1"]
                  [org.clojure/core.async "0.2.385"]
                  [org.clojure/java.jdbc "0.6.1"]
                  [com.avinium/extractor "0.0.2-SNAPSHOT"]
                  [com.mchange/c3p0 "0.9.2.1"]
                  [ubergraph "0.2.2"]
                  [org.postgresql/postgresql "9.4-1201-jdbc41"]]
  :javac-options ["-target" "1.6" "-source" "1.6" "-Xlint:-options"]
  :main com.avinium.dictionary.core
  :aot :all
  :source-paths ["src/clojure"]
  :test-paths ["test/clojure"]
  :profiles {:dev {:plugins [[lein-dotenv "RELEASE"]]}})

