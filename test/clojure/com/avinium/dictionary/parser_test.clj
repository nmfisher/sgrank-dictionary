(ns com.avinium.dictionary.parser-test
  (:gen-class)
  (:require [clojure.test :refer :all]
            [com.avinium.dictionary.parser :refer :all]
            [com.avinium.dictionary.dict :as dict]
            [clojure.data :refer :all]
            [com.avinium.dictionary.db-fixture :as dbfixture]
            [com.avinium.dictionary.file-fixture :as filefixture]
            [clojure.core.async :as async :refer [>! <! >!! <!! go go-loop chan buffer close! thread alts! alts!! timeout ]]))

(deftest test-parse-file-without-threshold
  "parse a file into all 1-grams and 2-grams"
    (let [ src (filefixture/file-setup-fixture)
           parsed (parse src { :splitRegex (re-pattern #"'s|[^\w-&]") :minThreshold 1 :concurrentStreams 1 :nMin 1 :nMax 2 } )]
        (is (= [1 39] [(dict/corpus-size parsed) (dict/ngram-count parsed)]))
        (is (= [2 1] [(dict/tf parsed ["four"]) (dict/df parsed ["four"])]))
        (is (= [1 1] [(dict/tf parsed ["four" "score"]) (dict/df parsed ["four" "score"])]))
        (is (= [1 1] [(dict/tf parsed ["Four" "score"]) (dict/df parsed ["Four" "score"])]))
        (is (= [1 1] [(dict/tf parsed ["four" "fathers"]) (dict/df parsed ["four" "fathers"])]))))

(deftest test-parse-file-with-threshold
  "parse a file into all 1-grams and 2-grams that appear more than once"
  (let [ src (filefixture/file-setup-fixture)
         parsed (parse src { :splitRegex (re-pattern #"'s|[^\w-&]") :minThreshold 2 :concurrentStreams 1 :nMin 1 :nMax 2 } )]
        (is (= 10 (dict/ngram-count parsed)))
        (is (= [2 1] [(dict/tf parsed ["four"]) (dict/df parsed ["four"])]))
        (is (= [0 0] [(dict/tf parsed ["four" "fathers"]) (dict/df parsed ["four" "fathers"])]))))

(deftest test-parse-file-keep-positions
  "parse a file, keeping all n-gram positions"
  (let [ src (filefixture/file-setup-fixture)
         parsed (parse src { :splitRegex (re-pattern #"'s|[^\w-&]") :minThreshold 2 :concurrentStreams 1 :nMin 1 :nMax 2 :keepPositions true} )]
        (is (= [8 3] (get-in parsed [:ngrams ["four"] :positions])))))

(deftest test-parse-directory
  "recursively parse a directory containing multiple files"
  (let [testChannel (timeout 1000)
        resultsChannel (go (loop [dict1 (<!! testChannel)] (let [dict2 (<!! testChannel)] (if (not (nil? dict2)) (recur (dict/mergeLR dict1 dict2)) dict1))))
        queue (recurse (filefixture/directory-setup-fixture) { :splitRegex (re-pattern #"'s|[^\w-&]") :minThreshold 1 :concurrentStreams 1 :nMin 1 :nMax 2 } testChannel)
        unused (close! testChannel)
        parsed (<!! resultsChannel)]
    (is (= [10 390] [(dict/corpus-size parsed) (dict/ngram-count parsed)]))
    (is (= [20 10] [(dict/tf parsed ["four"]) (dict/df parsed ["four"])]))
    (is (= [10 10] [(dict/tf parsed ["four" "score"]) (dict/df parsed ["four" "score"])]))
    (is (= [10 10] [(dict/tf parsed ["four" "fathers"]) (dict/df parsed ["four" "fathers"])]))))

(deftest test-parse-directory-no-channel
  "recursively parse a directory without a channel"
  (let [parsed (recurse (filefixture/directory-setup-fixture) { :splitRegex (re-pattern #"'s|[^\w-&]") :minThreshold 1 :concurrentStreams 1 :nMin 1 :nMax 2 })]
    (is (= [10 390] [(dict/corpus-size parsed) (dict/ngram-count parsed)]))
    (is (= [20 10] [(dict/tf parsed ["four"]) (dict/df parsed ["four"])]))
    (is (= [10 10] [(dict/tf parsed ["four" "score"]) (dict/df parsed ["four" "score"])]))
    (is (= [10 10] [(dict/tf parsed ["four" "fathers"]) (dict/df parsed ["four" "fathers"])]))))

(use-fixtures :each dbfixture/db-setup-fixture)

