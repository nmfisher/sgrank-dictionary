(ns com.avinium.dictionary.core-test
  (:gen-class)
  (:require [com.avinium.dictionary.core :refer :all]
            [com.avinium.dictionary.dict :as dict]
            [clojure.test :refer :all]
            [clojure.data :refer :all]
            [com.avinium.dictionary.db-fixture :as dbfixture]
            [com.avinium.dictionary.file-fixture :as filefixture]
            [clojure.core.async :as async :refer [>! <! >!! <!! go chan buffer close! thread alts! alts!! timeout ]])
  (:import (java.nio.file Files))
  (:import (java.nio.file.attribute FileAttribute)))

(use-fixtures :each dbfixture/db-setup-fixture)

(deftest test-read-parse-write-with-interval!
  "Integration test - recursively parse directory to create dictionary and save to database"
  (let [dict (-main (filefixture/directory-setup-fixture) "--writeInterval" "1" "--minThreshold" "1" "--nMin" "1" "--nMax" "2" "--dbName" "sgrank-test")]
    (is (= [10 390] [(dict/corpus-size dict) (dict/ngram-count dict)]))
    (is (= [20 10] [(dict/tf dict ["four"]) (dict/df dict ["four"])]))
    (is (= [10 10] [(dict/tf dict ["four" "score"]) (dict/df dict ["four" "score"])]))
    (is (= [10 10] [(dict/tf dict ["Four" "score"]) (dict/df dict ["Four" "score"])]))
    (is (= [10 10] [(dict/tf dict ["four" "fathers"]) (dict/df dict ["four" "fathers"])]))))

(deftest test-read-parse-write-with-lower-case!
  "Integration test - recursively parse directory to create dictionary and save to database (lowercase)"
  (let [dict (-main (filefixture/directory-setup-fixture) "--writeInterval" "1" "--minThreshold" "1" "--nMin" "1" "--nMax" "2" "--dbName" "sgrank-test" "--lc")]
    (is (= [10 390] [(dict/corpus-size dict) (dict/ngram-count dict)]))
    (is (= [30 10] [(dict/tf dict ["four"]) (dict/df dict ["four"])]))
    (is (= [20 10] [(dict/tf dict ["four" "score"]) (dict/df dict ["four" "score"])]))
    (is (= [0 0] [(dict/tf dict ["Four" "score"]) (dict/df dict ["Four" "score"])]))
    (is (= [10 10] [(dict/tf dict ["four" "fathers"]) (dict/df dict ["four" "fathers"])]))))

(deftest test-read-parse-hdfs-write-with-interval!
  "Integration test - recursively parse HDFS file to create dictionary and save to database"
  (let [dict (-main "hdfs://localhost:8020/wiki.xml"
                    "--writeInterval" "1"
                    "--minThreshold" "1"
                    "--nMin" "1"
                    "--nMax" "2"
                    "--dbName"
                    "sgrank-test")]
    (is (= [2 112] [(dict/corpus-size dict) (dict/ngram-count dict)]))
    (is (= [1 1] [(dict/tf dict ["quick" "brown"]) (dict/df dict ["quick" "brown"])]))))

(run-tests)
