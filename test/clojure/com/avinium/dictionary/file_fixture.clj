(ns com.avinium.dictionary.file-fixture
  (:gen-class)
  (:import (java.io File))
  (:import (java.nio.file Files))
  (:import (java.nio.file.attribute FileAttribute))
)

(defn load-db-spec []
  (read-string (slurp (str (System/getProperty "user.home") (java.io.File/separator) ".clojure" (java.io.File/separator) "test.db.config"))))

(defn file-setup-fixture []
  (let [f (.toString (Files/createTempFile "temp-file-name" ".txt" (make-array FileAttribute 0)))]
    (spit f "Four score and four score years ago our four fathers brought forth on this continent a new nation years ago")
    f))

(defn directory-setup-fixture []
  (let
    [d (Files/createTempDirectory "dict-test-dir" (make-array FileAttribute 0))
     fs (repeatedly 10 #(Files/createTempFile d "temp-file-name" ".txt" (make-array FileAttribute 0)))]
     (doseq [file fs] (spit (.toString file) "Four score and four score years ago our four fathers brought forth on this continent a new nation years ago"))
    (.toString d)))




