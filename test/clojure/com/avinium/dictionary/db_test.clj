(ns com.avinium.dictionary.db-test
  (:gen-class)
  (:require [com.avinium.dictionary.db :refer :all]
            [clojure.test :refer :all]
            [clojure.data :refer :all]
            [com.avinium.dictionary.db-fixture :as dbfixture]
            [com.avinium.dictionary.core :as core]
            [com.avinium.dictionary.dict :as dict])
  (:import (java.io File)))

(use-fixtures :each dbfixture/db-setup-fixture)

(deftest test-increment-corpus
  (is (= 1 (do (increment-corpus 1) (corpus-size)))
    "Increment the corpus count"))

(deftest test-increment-ngram-count
  (is (= 1 (do (increment-ngram-count 1) (ngram-count)))
    "Increment the word count"))

(deftest test-insert-ngram
  (is (= {:id 1, :content "some|word", :df 1, :tf 2, :length 2} (insert-ngram ["some" "word"] 1 2))
    "Insert a new ngram"))

(deftest test-insert-or-update-ngram
  (is (= {:id 1, :content "some|word" :df 1, :tf 2, :length 2} (insert-or-update-ngram ["some" "word"] 1 2))
    "Insert or update an ngram"))
