(ns com.avinium.dictionary.dict-test
  (:gen-class)
  (:require [clojure.data :refer :all]
            [clojure.test :refer :all]
            [com.avinium.dictionary.dict :refer :all]
            [com.avinium.dictionary.core :as core]
            [com.avinium.dictionary.db-fixture :as dbfixture]
            [com.avinium.dictionary.db :as db])
  (:import (java.io File)))

(use-fixtures :each dbfixture/db-setup-fixture)

(deftest test-lookup
  "Create a dictionary in memory and verify its ngrams"
  (let
    [dict (create 1 { ["some"] {:id 0 :df 1 :tf 1 } ["word"] { :id 1 :df 1 :tf 2 } })]
    (is (= [1 1] [(df dict ["some"]) (tf dict ["some"])]))
    (is (= [1 3] [(corpus-size dict) (ngram-count dict)]))))

(deftest test-load-dict
  "Create a dictionary in database and verify its ngrams and entry pairs"
  (do
    (db/increment-corpus 1)
    (db/increment-ngram-count 2)
    (db/insert-or-update-ngram ["foo" "bar"] 11 13)
    (db/insert-or-update-ngram ["bar" "foo"] 1 4)
    (db/insert-or-update-ngram ["foobar" "you"] 2 6)
    (let
      [dict (load-dict)]
        (is (= [1 23] [(corpus-size dict) (ngram-count dict)]))
        (is (= [11 13] [(df dict ["foo" "bar"]) (tf dict ["foo" "bar"])]))
        (is (= [1 4] [(df dict ["bar" "foo"]) (tf dict ["bar" "foo"])]))
        (is (= [2 6] [(df dict ["foobar" "you"]) (tf dict ["foobar" "you"])])))))

(deftest test-reduce-ngrams
  (is
    (let
      [dict1
       (create 1 { ["some"] { :id 0 :df 5 :tf 15  } ["word"] { :id 1 :df 4 :tf 27 } })
       dict2
       (reduce-ngrams dict1 (fn [attrs] (merge attrs {:df 77 })))]
      (is (= [1 42] [(corpus-size dict2) (ngram-count dict2)]))
      (is (= [77 15] [(df dict2 ["some"]) (tf dict2 ["some"])]))
      (is (= [77 27] [(df dict2 ["some"]) (tf dict2 ["word"])])))
    "reduce-ngrams applies a function to all ngrams in the dictionary"))

(deftest test-mergeLR
  "Merge two dictionaries left to right with partial shared ngrams"
  (let
    [merged
     (mergeLR
       (create 1 { ["some"] { :id 0 :df 1 :tf 2  } ["word"] { :id 1 :df 5 :tf 6 }})
       (create 1 { ["some"] { :id 0 :df 3 :tf 4 } ["anotherword"] { :id 1 :df 7 :tf 8 } })
       true true)]
    (is (= [2 20] [(corpus-size merged) (ngram-count merged)]))
    (is (= 4 (df merged ["some"])))
    (is (= 6 (tf merged ["some"])))
    (is (= 5 (df merged ["word"])))
    (is (= 6 (tf merged ["word"])))
    (is (= 7 (df merged ["anotherword"])))
    (is (= 8 (tf merged ["anotherword"])))))


(deftest test-mergeLR-nosum-doc-freqs
  "Merge two dictionaries left to right without summing document frequencies (takes the doc frequencies from the right)"
  (let
    [merged
     (mergeLR
       (create 1 { ["some"] { :id 0 :df 1 :tf 2  }})
       (create 1 { ["some"] { :id 0 :df 3 :tf 4 }})
      true false)]
        (is (= 2 (corpus-size merged)))
        (is (= 6 (ngram-count merged)))
        (is (= 3 (df merged ["some"])))))

(deftest test-mergeLR-nosum-corpus
  "Merge two dictionaries left to right without summing corpus size (takes the corpus size from the right)"
  (let
    [merged
     (mergeLR
       (create 1 { ["some"] { :id 0 :df 1 :tf 2  }})
       (create 1 { ["some"] { :id 0 :df 3 :tf 4 }})
      false true)]
        (is (= 1 (corpus-size merged)))
        (is (= 6 (ngram-count merged )))))

(deftest test-write-merge!-with-existing
  "Write then read a dictionary to/from a database with preexisting ngrams"
    (do
      (write-merge! (create 1 { ["some"] {:id 0 :df 1 :tf 2 } ["word"] { :id 1 :df 3 :tf 4 }}))
      (write-merge! (create 1 { ["some"] {:id 0 :df 1 :tf 2 } ["word"] { :id 1 :df 3 :tf 4 }}))
      (is (= 2 (df (load-dict) ["some"])))
      (is (= 4 (tf (load-dict) ["some"])))
      (is (= 6 (df (load-dict) ["word"])))
      (is (= 8 (tf (load-dict) ["word"])))))
