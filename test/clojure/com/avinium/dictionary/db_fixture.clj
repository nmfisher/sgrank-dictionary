(ns com.avinium.dictionary.db-fixture
  (:gen-class)
  (:require [clojure.java.jdbc :as sql]
            [com.avinium.dictionary.db :as db])
  (:import (java.io File))
)

(defn load-db-spec []
  (read-string (slurp (str (System/getProperty "user.home") (java.io.File/separator) ".clojure" (java.io.File/separator) "test.db.config"))))

(defn db-setup-fixture [f]
  (let
    [dbSpec (load-db-spec)]
    (db/init dbSpec false)
    (db/reset-all!)
    (f)))
