(in-ns 'com.avinium.dictionary.parser)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Split a line into its component words based on splitRegex
(defn split-line
  [target options]
  (->> (:splitRegex options)
       (str/split target)
       (filter #(not (str/blank? %1)))
       ((fn [split] (if (:lc options)
          (map str/lower-case split)
          split)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Split a document into its component words
(defn split-to-word-list
  [rdrCoercable options]
  (with-open
    [rdr (io/reader rdrCoercable)]
    (->> (line-seq rdr)
         (mapcat #(split-line % options))
         (into []))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
(defn- freqPos
  [partitioned]
  (->> partitioned
    (reduce (fn [[accum idx] part]
         [(assoc! accum part
                  { :frequency (inc (get-in accum [part :frequency] 0))
                    :positions (cons idx (get-in accum [part :positions] []))})
          (inc idx)])
     [(transient {}) 0])
    (first)
     (persistent!)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Turn a list of single words into an "TF map" where
;   K = word tuple
;   V = { :id - ID, set to nil
;         :df - document frequency (initially set to zero, will be incremented once the stream in which this word exist is complete)
;         :tf - absolute frequency (incremented each time this word is encountered in the word list)
(defn convert-to-tf-map
  [wordList nMin nMax keepPositions]
  (let [freqFn (if keepPositions freqPos frequencies)]
  (->(comp
       (if keepPositions
           #(into {} (for [[k v] %] [(vec k) { :tf (:frequency v) :df 1 :positions (:positions v)}]))
           #(into {} (for [[k v] %] [(vec k) { :tf v :df 1}])))
         freqFn
         #(partition %1 1 wordList))
      (map)
      (transduce merge (range nMin (inc nMax))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
(defn- to-dict
  [rdrCoercable options]
  (let
    ; split the document into a list of single words
    [wordList (split-to-word-list rdrCoercable options)
     ; convert the word list into a tf map
     tfMap (convert-to-tf-map wordList (:nMin options) (:nMax options) (:keepPositions options))]
    (dict/create 1 tfMap)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
(defn- do-threshold-cull
  [dict options]
  (->> (dict/ngrams dict :when #(>= (dict/tf dict %1) (:minThreshold options)))
       (dict/create 1)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
(defn parse
  [inputStream options]
  (let [dict (to-dict inputStream options)]
    (if (= 0 (dict/ngram-count dict))
      (println "Warning : file did not contain any parsable words (is this an unknown format or possibly a binary file?)")
      (do-threshold-cull dict options))))
