(in-ns 'com.avinium.dictionary.parser)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
(defn- parseFile
  [path options dictChannel]
  ; use a content extractor will separate a file into separate streams
  (let [extracted (extractor/extract (if (instance? File path) (.toString path) path))]
    (println "Finished stream extraction")
    ; if a channel was provided, parse each stream into an individual dictionary and place onto channel
    (if dictChannel
        (doseq [stream extracted]
          (let [parsed (parse stream options)]
            (if (and parsed (false? (>!! dictChannel parsed)))
                (throw (Exception. "Channel was prematurely closed...")))))
        ; otherwise, reduce and merge
      (reduce (fn [accum stream] (dict/mergeLR accum (parse stream options))) (dict/create) extracted))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
(defn recurse
  ([path options]
    (recurse path options nil))
  ([path options dictChannel]
    ; if it's a subdir, recurse downwards
     (if (.isDirectory (io/file path))
       (do
         (println "Recursing into directory " path)
         ; if a channel is not provided, we will reduce and merge every dictionary from every file
         (if dictChannel
             (doseq [path (.listFiles (io/file path))] (recurse path options dictChannel))
             (reduce (fn [accum path] (dict/mergeLR accum (recurse path options dictChannel))) (dict/create) (.listFiles (io/file path)))))
   ; otherwise, parse the file no files are found, the given path is itself a file
       (do (println "Parsing file " path)
           (parseFile path options dictChannel)))))
