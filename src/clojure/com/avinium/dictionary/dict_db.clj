(in-ns 'com.avinium.dictionary.dict)

(defn- load-ngrams
  []
  (->> (db/list-ngrams)))

(defn load-dict []
  (create (db/corpus-size) (load-ngrams)))

(defn write-merge!
  [dict]
  (do
    (println "Writing dictionary to database")
    (db/increment-corpus (corpus-size dict))
    (db/increment-ngram-count (ngram-count dict))
    (doseq [[ngram {:keys [:df :tf]}] (ngrams dict)]
      (db/insert-or-update-ngram ngram df tf))
    (db/commit)))

(defn write-reset!
  [dict]
    (db/reset-all!)
    (write-merge! dict))

(defn listen
  [channel options]
  (go
     (loop [dict1 (<! channel) counter 0]
       (if (not (nil? dict1))
         (if-let [dict2 (<! channel)]
           (if (> counter (:writeInterval options))
             (do (write-merge! (mergeLR dict1 dict2))
                 (recur (create) 0))
             (do (recur (mergeLR dict1 dict2) (inc counter))))
           (do (write-merge! dict1) dict1))))))
