(ns com.avinium.dictionary.db
  (:gen-class)
  (:require [clojure.java.jdbc :as sql]
            [clojure.string :as str])
  (:import com.mchange.v2.c3p0.ComboPooledDataSource))

(def db-spec (atom {}))

(defn pool
  [spec]
  (let [cpds (doto (ComboPooledDataSource.)
               (.setDriverClass (:classname spec))
               (.setJdbcUrl (str "jdbc:" (:subprotocol spec) ":" (:subname spec)))
               (.setUser (:user spec))
               (.setPassword (:password spec))
               ;; expire excess connections after 30 minutes of inactivity:
               (.setMaxIdleTimeExcessConnections (* 30 60))
               ;; expire connections after 3 hours of inactivity:
               (.setMaxIdleTime (* 3 60 60)))]
    {:datasource cpds}))

(def pooled-db (delay (pool (deref db-spec))))

(defn db-connection [] @pooled-db)

(defn create-tables []
  (do
    (try
      (sql/db-do-commands (db-connection)
                          (sql/create-table-ddl
                            :statistics
                            [[:corpus_size :bigint "NOT NULL"]
                             [:ngram_count :bigint "NOT NULL"]]))
      (catch Exception e (println (str e))))
    (try
      (sql/db-do-commands (db-connection)
                          (sql/create-table-ddl
                            :ngrams
                            [[:id :serial "PRIMARY KEY"]
                             [:content :text "NOT NULL" "UNIQUE"]
                             [:df :bigint "NOT NULL"]
                             [:tf :bigint "NOT NULL"]
                             [:length :int "NOT NULL"]]))
      (catch Exception e (println (str (.getNextException e)))))))

(defn init [spec createTables?]
  (do
    (swap! db-spec (fn [& args] spec))
    (if createTables?
      (create-tables))))

(defn reset-all! []
  (try (sql/db-do-commands (db-connection) (sql/drop-table-ddl :ngrams_words)) (catch Exception e))
  (try (sql/db-do-commands (db-connection) (sql/drop-table-ddl :ngrams [])) (catch Exception e))
  (try (sql/db-do-commands (db-connection) (sql/drop-table-ddl :words [])) (catch Exception e))
  (try (sql/db-do-commands (db-connection) (sql/drop-table-ddl :statistics [])) (catch Exception e))
  (create-tables))

(load "db_statistics")
(load "db_ngrams")

