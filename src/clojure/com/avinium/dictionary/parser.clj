(ns com.avinium.dictionary.parser
  (:gen-class)
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.tools.cli :refer [parse-opts]]
            [clojure.core.async :as async :refer [>! <! >!! <!! go go-loop chan buffer close! thread alts!   alts!! timeout ]]
            [com.avinium.dictionary.dict :as dict]
            [com.avinium.extractor.core :as extractor])
  (:import java.io.File)
  (:import java.net.URL))

(load "parser_single_stream")
(load "parser_recurser")
