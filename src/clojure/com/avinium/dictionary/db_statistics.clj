(in-ns 'com.avinium.dictionary.db)

(defn init-statistics
  []
  (let
    [initial { :corpus_size 0 :ngram_count 0 }]
    (sql/insert!
      (db-connection)
        :statistics initial)
    initial))

(defn corpus-size
  []
  (let
    [statistics
       (sql/query
         (db-connection)
         ["SELECT corpus_size FROM statistics"])]
    (if (empty? statistics)
      (get (init-statistics) :corpus_size)
      (get (first statistics) :corpus_size))))

(defn increment-corpus [size]
  (let
    [corpusSize (corpus-size)]
      (sql/update!
        (db-connection)
        :statistics
        { :corpus_size (+ corpusSize size) }
        ["1 = 1"])))

(defn ngram-count
  []
  (let
    [statistics
     (sql/query
       (db-connection)
       ["SELECT ngram_count FROM statistics"])]
    (if (empty? statistics)
      (get (init-statistics) :ngram_count)
      (get (first statistics) :ngram_count))))

(defn increment-ngram-count
  [size]
  (let
    [ngramCount (ngram-count)]
      (sql/update!
        (db-connection)
        :statistics
        { :ngram_count (+ ngramCount size) }
        ["1 = 1"])))
