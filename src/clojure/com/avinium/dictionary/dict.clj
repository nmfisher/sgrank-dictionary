(ns com.avinium.dictionary.dict
  (:gen-class)
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [com.avinium.dictionary.db :as db]
            [clojure.core.async :as async :refer [>! <! >!! <!! go go-loop chan buffer close! thread alts! alts!! timeout ]]))

; { :corpusSize
;    :ngramCount
;    :ngrams
; }

(defn corpus-size
    [dict]
    (or (:corpusSize dict) 0))

(defn ngram-count
    [dict]
    (or (:ngramCount dict) 0))

(defn ngrams
  ([dict]
    (:ngrams dict))
  ([dict kw pred]
    (if (= :when kw)
      (into {} (for [[k v] (ngrams dict)] (if (pred k) [k v]))))))

(defn reduce-ngrams
  ([dict f]
   (assoc dict :ngrams
     (into {} (for [[k v] (ngrams dict)] [ k (f v) ])))))

(defn ngram [dict n]
  (get (ngrams dict) n))

(defn df
  [this n]
  (or (:df (ngram this n)) 0))

(defn tf
  [this n]
  (or (:tf (ngram this n)) 0))

(defn id
  [this n]
  (or (:id (ngram this n)) nil))

(defn add-ngram
  ([dict ngram]
   (add-ngram dict ngram {:id nil :df 0 :tf 0 }))
  ([dict ngram attrs]
  (assoc-in dict [:ngrams ngram] attrs)))

(defn has-ngram?
  [dict ngram]
  (contains? (ngrams dict) ngram))

(defn add-all
  [dict ngrams]
  (reduce
    #(if (has-ngram? %1 %2)
       %1
       (add-ngram %1 (keyword %2)))
    dict
    ngrams))

(declare load-dict)
(declare merge-pairs)
(declare write-merge-ngram-pairs!)

(defn- merge-ngram
  [sumDocFreq? entry1 entry2]
    { :id (or (:id entry1) (:id entry2))
      :df (cond->> (:df entry1)
                  sumDocFreq? (+ (:df entry2))
                  (false? sumDocFreq?) (or (:df entry2) 0))
      :tf (+ (:tf entry1) (:tf entry2)) })

(defn- merge-ngrams
  [dict1 dict2 sumDocFreq?]
  { :ngrams (merge-with (partial merge-ngram sumDocFreq?) (ngrams dict1) (ngrams dict2)) })

(defn mergeLR
  ([dict1 dict2]
   (mergeLR dict1 dict2 true true))

  ([dict1 dict2 sumCorpus? sumDocFreq?]
   ;; merge the counts for the ngrams
  (-> (merge-ngrams dict1 dict2 sumDocFreq?)
      ; if sumCorpus is specified, the corpusSize of the merged dictionary will be the sum of the two parts
      (assoc :corpusSize
        (if (true? sumCorpus?) (+ (corpus-size dict1) (corpus-size dict2))
          ; else use the max
          (max (corpus-size dict2) (corpus-size dict1))))
       ; sum the word counts
      (assoc :ngramCount (+ (ngram-count dict1) (ngram-count dict2))))))

(defn create
  ([]
   (create 0 {}))
  ([corpusSize ngrams]
   { :corpusSize corpusSize :ngramCount (reduce #(+ %1 (:tf %2)) 0 (vals ngrams)) :ngrams ngrams}))

(load "dict_db")

