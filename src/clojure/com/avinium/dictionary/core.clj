(ns com.avinium.dictionary.core
  (:gen-class)
  (:require [clojure.string :as str]
            [clojure.tools.logging :as log]
            [clojure.java.io :as io]
            [clojure.tools.cli :refer [parse-opts]]
            [com.avinium.dictionary.db :as db]
            [com.avinium.dictionary.dict :as dict]
            [com.avinium.dictionary.parser :as parser]
            [clojure.core.async :as async :refer [>! <! >!! <!! go chan buffer close! thread alts! alts!! timeout ]]))

(def cli-options
  [
    ["-X" "--DROP" "whether the database will be reset before files are parsed"]
    ["-r" "--splitRegex REGEX" "regex to tokenize the file to parse"
     :parse-fn #(re-pattern %)
     :default (re-pattern #"'s|[^\w-&]")]
    ["-m" "--minThreshold FREQUENCY" "a word in a document must appear at least this number of times to be kept in the dictionary"
     :parse-fn #(Integer/parseInt %)
     :default 1]
    [nil "--sparkMaster MASTER" "address of the spark master"]
     ;:default "local[*]"]
    [nil "--writeInterval INTERVAL" "the number of files that will be parsed before the dictionary is written out to the specified destination"
     :default 10
     :parse-fn #(Integer/parseInt %)
      :validate [#(< 0 % 0x20000) "Must be a number between 0 and 131072"]]
    [nil "--keepPositions" "whether or not to record the positions of every ngram. Only applicable to single file parsing."
     :default false]
    [nil "--concurrentStreams CONCURRENT" "the number of streams in a single file that may be parsed concurrently"
     :default 4
     :parse-fn #(Integer/parseInt %)
      :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
    [nil "--dbSubProtocol PROTOCOL" "Database JDBC subprotocol"
     :default "postgresql"]
    [nil "--dbHost HOST" "Database server hostname"
     :default "localhost"]
    [nil "--dbPort PORT" "Database server port"
     :default 5432]
    [nil "--dbName NAME" "Database name"]
    [nil "--dbDriver TYPE" "Database driver"
      :default "org.postgresql.Driver"]
    [nil "--dbUsername USERNAME" "Database username"]
    [nil "--dbPassword PASSWORD" "Database password"]
    [nil "--nMin SIZE" "the smallest length n-gram to collect"
     :parse-fn #(Integer/parseInt %)
     :default 1
     :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
    [nil "--nMax SIZE" "the largest length n-gram to collect"
     :parse-fn #(Integer/parseInt %)
     :default 1
     :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
    [nil "--lc" "if true, convert n-grams to lowercase"]
    ["-h" "--help"]])

(defn- usage [options-summary]
  (->> ["Creates/reads a dictionary from a file, "
        ""
        "Usage: java -jar dictionary.jar [targetfile]"
        ""
        "Options:"
        options-summary]
       (str/join \newline)))

(defn- error-msg [errors]
  (str "The following errors occurred while parsing your command:\n\n"
       (str/join \newline errors)))

(defn- exit [status msg]
  (println msg)
  (System/exit status))

(defn init-db [db-spec]
  (println "Initializing database with spec : " db-spec)
  (db/init db-spec true)
  (try
    (db/corpus-size)
    (catch Exception e
      (exit 1 (str e)))))

(defn parse-db-spec [options]
  {
    :classname (get options :dbDriver)
    :subprotocol (get options :dbSubProtocol)
    :subname (str "//" (get options :dbHost) ":" (get options :dbPort) "/" (get options :dbName))
    :user (get options :dbUser)
    :password (get options :dbPassword)})

(defn- start-parse [targetPath options]
    (let [dictChannel  (chan)
          dictChannelComplete (dict/listen dictChannel options)]
      (if (nil? (:sparkMaster options))
          (parser/recurse targetPath options dictChannel))
      (close! dictChannel)
      (<!! dictChannelComplete)))

(defn -main [& args]
  (let
    [{:keys [options arguments errors summary]}
       (parse-opts args cli-options)

     dbSpec
       (parse-db-spec options)

     targetPath
       (if (or (nil? arguments) (= 0 (count arguments)))
           nil
           (nth arguments 0))]

    (cond
        (:help options)
          (exit 0 (usage summary))

        errors
          (exit 1 (error-msg errors)))

    (init-db dbSpec)

    (if (get options :DROP)
      (do
        (println "\r\n *** WARNING *** \r\n the --DROP option will drop all preexisting database tables and records before files are parsed. Use with care! Are you sure you want to proceed (Y/N)")
        (flush)
        (let [confirm (read-line)]
          (if (#{"Y" "y"} confirm)
            (db/reset-all!)
            (exit 0 "Exiting...")))))
    (if (nil? targetPath)
      (exit 1 "No path specified...")
      (do
          (start-parse targetPath options)
          (-> (dict/load-dict)
          ((fn [dict] (do
              (println "Dictionary stats : Corpus (" (dict/corpus-size dict) ") Ngram Count (" (dict/ngram-count dict) ") ")
               dict))))))))
