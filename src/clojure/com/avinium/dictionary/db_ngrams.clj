(in-ns 'com.avinium.dictionary.db)

(defn- to-saveable
  [tuple]
  (str/join "|" tuple))

(defn- from-saveable
  [stringified]
  (str/split stringified #"\|"))

(defn get-ngram
  [tuple]
  (first (sql/query (db-connection) ["SELECT ngrams.*  FROM ngrams where content=?" (to-saveable tuple)])))

(defn get-word
  [word]
  (let [persistent (sql/query (db-connection) ["SELECT * FROM words WHERE word= ?" word])]
    (if (empty? persistent)
      nil
      (first persistent))))

(defn list-ngrams
  []
  (->> (sql/query (db-connection) ["SELECT * FROM ngrams ORDER BY ngrams.id"])
       (reduce (fn [accum ngram] (assoc accum (from-saveable (:content ngram)) (dissoc ngram :content))) {})))

(defn insert-ngram
  [tuple df tf]
  (first (sql/insert! (db-connection) :ngrams { :df df :tf tf :length (count tuple) :content (to-saveable tuple) } {:transaction false})))

(defn update-ngram
  [tuple df tf]
  (let [ngram (get-ngram tuple)]
     (sql/update! (db-connection) :ngrams { :df (+ (:df ngram) df) :tf (+ (:tf ngram) tf) } ["id = ?" (:id ngram)] {:transaction false})))

(defn insert-or-update-ngram
  [tuple df tf]
  (->
    (get-ngram tuple)
    ((fn [ngram]
      (if (nil? ngram)
          (insert-ngram tuple df tf)
          (update-ngram tuple df tf))))))

(defn commit
  []
  (sql/execute! (db-connection) ["COMMIT"]))

(defn db-setup-fixture [f]
  (init (read-string (slurp (str (System/getProperty "user.home") (java.io.File/separator) ".clojure" (java.io.File/separator) "test.db.config"))))
  (reset-all!)
  (f))
